import machine
import ssd1306
from pyb import ADC, Pin, Timer
import time
import gfx

Pin(Pin.cpu.A1, mode=Pin.IN)

led = pyb.LED(1)
i2c = machine.SoftI2C(scl=machine.Pin('B8'), sda=machine.Pin('B9'))
display = ssd1306.SSD1306_I2C(128, 32, i2c)

rtc = machine.RTC()
rtc.datetime((2022, 03, 16, 21, 57, 00, 0, 0))

MAX_HISTORY = 200
TOTAL_BEATS = 20

gfx = gfx.GFX(128, 32, display.pixel)
last_y = 4

RECALIBRATION_ERROR = [
    0,0,0,1,1,0,0,0,
    0,0,0,1,1,0,0,0,
    0,0,0,1,1,0,0,0,
    0,0,0,1,1,0,0,0,
    0,0,0,1,1,0,0,0,
    0,0,0,0,0,0,0,0,
    0,0,0,1,1,0,0,0,
    0,0,0,1,1,0,0,0
]

def stringify(date):
    if date < 10:
        return "0" + str(date)
    return str(date)

def getCurrentTime():
    currentTime = time.gmtime(time.time())

    year = str(currentTime[0])
    month = stringify(currentTime[1])
    day = stringify(currentTime[2])

    hour = stringify(currentTime[3])
    minutes = stringify(currentTime[4])
    seconds = stringify(currentTime[5])

    date_1 = day + "/" + month + "/" + year
    date_2 = hour + ":" + minutes + ":" + seconds

    return (date_1, date_2)

def show_recalibrate():
    display.text("[!]", 0, 9)

def show_missing_finger():
    display.text("[?]", 22, 9)

def refresh(bpm, beat, v, minima, maxima):
    global last_last_y
    global last_y
    y = None

    display.scroll(-1, 0)

    should_recalibrate = False
    delta = maxima - minima

    if delta > 0:
        if bpm and bpm > 27 and bpm < 400:
            y = 28 - int(10 * (v - minima) / (maxima - minima))
            gfx.line(125, last_y, 126, y, 1)
            last_last_y = last_y
            last_y = y
        else:
            should_recalibrate = True

    display.scroll(-1, 0)
    gfx.fill_rect(0, 0, 128, 16, 0)

    if bpm:
        if bpm <= 27:
            display.text("%d bpm*" % bpm, 0, 0)
        else:
            display.text("%d bpm" % bpm, 0, 0)

    if should_recalibrate:
        show_recalibrate()

    epsilon = 2
    if y and abs(last_last_y - y) < epsilon:
        print("Last last y: " + str(abs(last_last_y - y)))
        show_missing_finger()

    timeTuple = getCurrentTime()
    display.text(timeTuple[0], 49, 9)
    display.text(timeTuple[1], 64, 0)

    display.show()


def calculate_bpm(beats):
    beats = beats[-TOTAL_BEATS:]
    beat_time = beats[-1] - beats[0]
    if beat_time:
        bpm = (len(beats) / (beat_time)) * 60
        return bpm

def detect():
    history = []
    beats = []
    beat = False
    bpm = None

    display.fill(0)

    while True:
        v = ADC("A1").read()
        history.append(v)

        history = history[-MAX_HISTORY:]

        minima, maxima = min(history), max(history)

        # Calculate different threshold value
        threshold_on = (minima + maxima * 3) // 4
        threshold_off = (minima + maxima) // 2

        if v > threshold_on and beat == False:
            beat = True
            beats.append(time.time())
            bpm = calculate_bpm(beats)

        if v < threshold_off and beat == True:
            beat = False

        refresh(bpm, beat, v, minima, maxima)

detect()
