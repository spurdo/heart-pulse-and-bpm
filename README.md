# Micropython heart pulse and BPM

This code allows you to display a live graph of your heart pulse, as well as your current BPM right on the OLED display of your MicroPython board.
You can get started right away by moving all the files in src/ to your Micropython environment, and reconnecting your board to your PC.

## For a serial prompt:
 - Windows: you need to go to 'Device manager', right click on the unknown device,
   then update the driver software, using the 'pybcdc.inf' file found in the misc/ folder.
   Then use a terminal program like Hyperterminal or putty.
 - Mac OS X: use the command: screen /dev/tty.usbmodem*
 - Linux: use the command: screen /dev/ttyACM0

Please visit http://micropython.org/help/ for further help.
